from unittest import TestCase, mock
import stager_access as stager


@mock.patch('stager_access.proxy')
class TextProxyGetSrmToken(TestCase):
    def test_get_srm_token(self, proxy_mock):
        stage_id = 135
        stager.get_srm_token(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_token.call_args.args[0], stage_id)

    def test_get_srm_token_return_token(self, proxy_mock):
        token = 'd49e386d:-483373315'
        proxy_mock.LtaStager.get_token.return_value = token
        actual = stager.get_srm_token(4)
        self.assertEqual(token, actual)
