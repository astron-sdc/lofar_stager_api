import unittest.mock as mock
from unittest import TestCase
import stager_access as stager


@mock.patch('stager_access.proxy')
class TestProxyGetSurlsOnline(TestCase):
    def test_get_surls_online(self, proxy_mock):
        stage_id = 12
        stager.get_surls_online(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_staged_urls.call_args.args[0], stage_id)

    def test_get_surls_online_return_one_surl(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:3233/pnfs/grid.sara.nl/data/dummy.tar']
        proxy_mock.LtaStager.get_staged_urls.return_value = surls_requested
        actual = stager.get_surls_online(4)
        self.assertEqual(surls_requested, actual)

    def test_get_surls_online_return_multiple_surls(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:3233/pnfs/grid.sara.nl/data/dummy.tar',
                           'srm://srm.grid.sara.nl:3233/pnfs/grid.sara.nl/data/dummy2.tar']
        proxy_mock.LtaStager.get_staged_urls.return_value = surls_requested
        actual = stager.get_surls_online(8)
        self.assertEqual(surls_requested, actual)

