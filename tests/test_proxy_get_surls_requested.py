import unittest.mock as mock
from unittest import TestCase
import stager_access as stager


@mock.patch('stager_access.proxy')
class TestProxyGetSurlsRequested(TestCase):
    def test_get_surls_requested(self, proxy_mock):
        stage_id = 7
        stager.get_surls_requested(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_requested_urls.call_args.args[0], stage_id)

    def test_get_surls_requested_return_one_surl(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/dummy.tar']
        proxy_mock.LtaStager.get_requested_urls.return_value = surls_requested
        actual = stager.get_surls_requested(5)
        self.assertEqual(surls_requested, actual)

    def test_get_surls_requested_return_multiple_surl(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/dummy.tar',
                           'srm://srm.grid.sara.nl:8443/pnfs/grid.sara.nl/data/dummy2.tar']
        proxy_mock.LtaStager.get_requested_urls.return_value = surls_requested
        actual = stager.get_surls_requested(5)
        self.assertEqual(surls_requested, actual)


@mock.patch('stager_access.proxy')
class TestProxyGetGridftpRequested(TestCase):
    def test_get_gridftp_requested(self, proxy_mock):
        stage_id = 7
        stager.get_gridftp_urls_requested(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_gridftp_urls.call_args.args[0], stage_id)

    def test_get_gridftp_urls_requested_return_one(self, proxy_mock):
        urls_requested = ['gsiftp://gridftp.grid.sara.nl/pnfs/grid.sara.nl/data/dummy.tar']
        proxy_mock.LtaStager.get_gridftp_urls.return_value = urls_requested
        actual = stager.get_gridftp_urls_requested(5)
        self.assertEqual(urls_requested, actual)

    def test_get_gridftp_urls_requested_return_multiple(self, proxy_mock):
        urls_requested = ['gsiftp://gridftp.grid.sara.nl/pnfs/grid.sara.nl/data/dummy1.tar',
                          'gsiftp://gridftp.grid.sara.nl/pnfs/grid.sara.nl/data/dummy2.tar']
        proxy_mock.LtaStager.get_gridftp_urls.return_value = urls_requested
        actual = stager.get_gridftp_urls_requested(5)
        self.assertEqual(urls_requested, actual)


@mock.patch('stager_access.proxy')
class TestProxyGetWebdavRequested(TestCase):
    def test_get_webdav_requested(self, proxy_mock):
        stage_id = 7
        stager.get_webdav_urls_requested(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_webdav_urls.call_args.args[0], stage_id)

    def test_get_webdav_urls_requested_return_one(self, proxy_mock):
        urls_requested = ['https://webdav.grid.surfsara.nl:2882/projectx/dummy.tar']
        proxy_mock.LtaStager.get_webdav_urls.return_value = urls_requested
        actual = stager.get_webdav_urls_requested(5)
        self.assertEqual(urls_requested, actual)

    def test_get_webdav_urls_requested_return_multiple(self, proxy_mock):
        urls_requested = ['https://webdav.grid.surfsara.nl:2882/projectx/dummy1.tar',
                          'https://webdav.grid.surfsara.nl:2882/projectx/dummy2.tar']
        proxy_mock.LtaStager.get_webdav_urls.return_value = urls_requested
        actual = stager.get_webdav_urls_requested(5)
        self.assertEqual(urls_requested, actual)