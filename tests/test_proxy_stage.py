import unittest.mock as mock
from unittest import TestCase
import stager_access as stager


@mock.patch('stager_access.proxy')
class TestProxyStage(TestCase):
    def test_stage_with_single_surl(self, proxy_mock):
        surl = 'single_surl'
        stager.stage(surl)
        self.assertEqual(proxy_mock.LtaStager.add_and_get_id.call_args.args[0], [surl])

    def test_stage_with_multiple_surls(self, proxy_mock):
        surls = ["surl1", "surl2", "surl3"]
        stager.stage(surls)
        self.assertEqual(proxy_mock.LtaStager.add_and_get_id.call_args.args[0], surls)

    def test_stage_send_notifications_default(self, proxy_mock):
        stager.stage('dummysurl')
        self.assertEqual(proxy_mock.LtaStager.add_and_get_id.call_args.args[1], True)

    def test_stage_send_notifications_true(self, proxy_mock):
        stager.stage('dummysurl', send_notifications=True)
        self.assertEqual(proxy_mock.LtaStager.add_and_get_id.call_args.args[1], True)

    def test_stage_send_notifications_false(self, proxy_mock):
        stager.stage('dummysurl', send_notifications=False)
        self.assertEqual(proxy_mock.LtaStager.add_and_get_id.call_args.args[1], False)

    def test_stage_return_stage_id(self, proxy_mock):
        stage_id = 5
        proxy_mock.LtaStager.add_and_get_id.return_value = stage_id
        actual = stager.stage('dummysurl')
        self.assertEqual(actual, stage_id)
