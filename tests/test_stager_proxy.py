import unittest
from unittest.mock import patch, mock_open, MagicMock
from requests import HTTPError
import datetime

from stager_proxy import StagerProxy


class TestStagerProxy(unittest.TestCase):

    def setUp(self):
        self.proxy = StagerProxy()

    @patch('builtins.print')
    def test_print_exception_and_exit(self, mock_print):
        with self.assertRaises(SystemExit):
            self.proxy.print_exception_and_exit(HTTPError())
        mock_print.assert_called_with("Invalid HTTP response, something is wrong with the server")

    @patch('stager_proxy.StagerProxy.create_proxy')
    def test_LtaStager(self, mock_create_proxy):
        mock_create_proxy.return_value = 'proxy_instance'
        result = self.proxy.LtaStager
        self.assertEqual(result, 'proxy_instance')
        self.assertEqual(self.proxy.proxy, 'proxy_instance')

    @patch('stager_proxy.StagerProxy.parse_credentials', return_value=(None, None, 'api_token', 'hostname'))
    @patch('stager_proxy.StagerProxy.from_json_api', return_value='json_api_proxy')
    @patch('builtins.print')
    def test_create_proxy_with_token(self, mock_print, mock_from_json_api, mock_parse_credentials):
        result = self.proxy.create_proxy()
        self.assertEqual(result, 'json_api_proxy')
        args, _ = mock_print.call_args
        self.assertIn("- stager_access: Found API token. Creating json api proxy", str(args), msg="Part of print output not found")

    @patch('stager_proxy.exists', return_value=True)
    @patch('builtins.open', new_callable=mock_open, read_data='key=value')
    @patch('stager_proxy.read_config_string', return_value={'my_key': 'my_value'})
    @patch('builtins.print')
    def test_parse_config_file(self, mock_print, mock_read_config_string, mock_open, mock_exists):
        result = self.proxy.parse_config_file("/fake/path")
        self.assertEqual(result, {'my_key': 'my_value'})

    @patch('os.path.expanduser', return_value='/fake/path')
    @patch('stager_proxy.StagerProxy.parse_config_file', return_value={'user': 'me', 'password': 'my_password', 'api_token': 'my_token', 'hostname': 'my_hostname'})
    @patch('builtins.print')
    def test_parse_credentials(self, mock_print, mock_parse_config_file, mock_expanduser):
        result = self.proxy.parse_credentials()
        self.assertEqual(result, ('me', 'my_password', 'my_token', 'my_hostname'))


class JsonApiStager:
    def __init__(self, graphql_uri, rest_uri, token):
        self.graphql_uri = graphql_uri
        self.rest_uri = rest_uri
        self.token = token


if __name__ == '__main__':
    unittest.main()
