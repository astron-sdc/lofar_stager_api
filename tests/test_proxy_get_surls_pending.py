import unittest.mock as mock
from unittest import TestCase
import stager_access as stager


@mock.patch('stager_access.proxy')
class TestProxyGetSurlsPending(TestCase):
    def test_get_surls_pending(self, proxy_mock):
        stage_id = 13
        stager.get_surls_pending(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_outstanding_urls.call_args.args[0], stage_id)

    def test_get_surls_pending_return_one_surl(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:1337/pnfs/grid.sara.nl/data/dummy.tar']
        proxy_mock.LtaStager.get_outstanding_urls.return_value = surls_requested
        actual = stager.get_surls_pending(3)
        self.assertEqual(surls_requested, actual)

    def test_get_surls_pending_return_multiple_surls(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:1337/pnfs/grid.sara.nl/data/dummy.tar',
                           'srm://srm.grid.sara.nl:1337/pnfs/grid.sara.nl/data/dummy2.tar']
        proxy_mock.LtaStager.get_outstanding_urls.return_value = surls_requested
        actual = stager.get_surls_pending(2)
        self.assertEqual(surls_requested, actual)

