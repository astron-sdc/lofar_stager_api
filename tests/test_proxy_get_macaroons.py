import unittest.mock as mock
from unittest import TestCase
import stager_access as stager

TEST_STAGE_ID = 7


@mock.patch('stager_access.proxy')
class TestProxyGetMacaroonsRequested(TestCase):

    def check_get_macaroons_requested(self, proxy_mock, macaroons):
        proxy_mock.LtaStager.get_macaroons.return_value = macaroons
        actual = stager.get_macaroons(TEST_STAGE_ID)
        self.assertEqual(macaroons, actual)

    def test_get_macaroons_requested(self, proxy_mock):
        stage_id = TEST_STAGE_ID
        stager.get_macaroons(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_macaroons.call_args.args[0], stage_id)

    def test_get_macaroons_requested_return_one(self, proxy_mock):
        self.check_get_macaroons_requested( proxy_mock, ['ThisIsAMacaroon'])

    def test_get_macaroons_requested_return_multiple(self, proxy_mock):
        self.check_get_macaroons_requested(proxy_mock, ['ThisIsAMacaroon', 'ThisIsAnotherMacaroon'])




