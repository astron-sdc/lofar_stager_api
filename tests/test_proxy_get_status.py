from unittest import TestCase, mock
import stager_access as stager


@mock.patch("stager_access.proxy")
class TestProxyGetStatus(TestCase):
    def test_get_status_stage_id(self, proxy_mock):
        stage_id = 5
        stager.get_status(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_status.call_args.args[0], stage_id)

    def test_get_status_return_status(self, proxy_mock):
        status = 'new'
        proxy_mock.LtaStager.get_status.return_value = status
        actual = stager.get_status(5)
        self.assertEqual(actual, status)
