import unittest.mock as mock
from unittest import TestCase
import stager_access as stager


@mock.patch('stager_access.proxy')
class TextProxyAbort(TestCase):
    def test_abort(self, proxy_mock):
        stage_id = 7
        stager.abort(stage_id)
        self.assertEqual(proxy_mock.LtaStager.abort.call_args.args[0], stage_id)

    def test_abort_return_result_True(self, proxy_mock):
        result = True
        proxy_mock.LtaStager.abort.return_value = result
        actual = stager.abort(5)
        self.assertEqual(actual, result)

    def test_abort_return_result_False(self, proxy_mock):
        result = False
        proxy_mock.LtaStager.abort.return_value = result
        actual = stager.abort(5)
        self.assertEqual(actual, result)
