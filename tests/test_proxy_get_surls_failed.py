import unittest.mock as mock
from unittest import TestCase
import stager_access as stager


@mock.patch('stager_access.proxy')
class TestProxyGetSurlsFailed(TestCase):
    def test_get_surls_failed(self, proxy_mock):
        stage_id = 17
        stager.get_surls_failed(stage_id)
        self.assertEqual(proxy_mock.LtaStager.get_failed_urls.call_args.args[0], stage_id)

    def test_get_surls_failed_return_one_surl(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:1337/pnfs/grid.sara.nl/data/dummy.tar']
        proxy_mock.LtaStager.get_failed_urls.return_value = surls_requested
        actual = stager.get_surls_failed(1)
        self.assertEqual(surls_requested, actual)

    def test_get_surls_failed_return_multiple_surls(self, proxy_mock):
        surls_requested = ['srm://srm.grid.sara.nl:1337/pnfs/grid.sara.nl/data/dummy.tar',
                           'srm://srm.grid.sara.nl:1337/pnfs/grid.sara.nl/data/dummy2.tar']
        proxy_mock.LtaStager.get_failed_urls.return_value = surls_requested
        actual = stager.get_surls_failed(9)
        self.assertEqual(surls_requested, actual)

