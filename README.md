# lofar_stager_api

If you use (part of) this software please attribute the use to ASTRON as indicated in the file NOTICE in the root of the source code repository.


## WARNING 

Current released version 2.0 (master) is to be used with the new LTA stager (stageit), it is not backwards compatible with the old LTA stager. Older versions of this script (i.e., 1.7 and older) have become obsolete.

* User documentation for stageit can be found at: https://sdc.astron.nl/stageit/static/documentation/docs/user-guide/advanced/api
* Release 2.0 can be found at: https://git.astron.nl/astron-sdc/lofar_stager_api/-/releases/2.0

## Version 2.0 (current) usage notes
Usage notes:

- You need an access token to the stageit api. Please refer to the user guide linked above to sign up and login to stageit. After logging in, a token can be obtained in one of two ways:
    - Visit https://sdc.astron.nl/stageit/api/staging/get-token
    - From anywhere in the application, click on your account name in the top right to access your profile. From your profile page, click the "Request token" button to receive a token.
- The token is valid indefinitely. Requesting a token multiple times will yield the same token.
- Make sure the token is available in your ~/.stagingrc file:
    - api_token=YOUR_TOKEN_HERE
    - remove the old username and password from the `.stagingrc` file
- The script is Python2 compatible, there is a Dockerfile available for Python2 testing in `./tests/docker`
- The requests library is a required dependency. If you care about Python2 compatability, you can use at most version 2.22.0 of requests. Otherwise, you can install any version (note: you can also `pip install -r 'requirements.txt'`, which will install version 2.22.0)

### Deprecated functions: Due to changes in the backend, some functions have been deprecated. Those can be called, but this will result in a warning and raise a `NotImplementedError`. The following tables shows the functions together with the rationale to deprecate them:
|function | reason |
| ------------- | ------------- |
| get_stuck_requests(min_retries, days_without_activity) | `stuck` status does not exist anymore |
| reschedule_on_status() | failed jobs reschedule automatically for a few times before failing, manual reschedule still possible|
| get_storage_info() | this is not really user functionality and has been superseded by views for the operators |
| reschedule_on_hold() | `on hold` status does not exist anymore |
| print_on_hold() | `on hold` status does not exist anymore |
| reschedule_aborted() | `aborted` status does not exist anymore |