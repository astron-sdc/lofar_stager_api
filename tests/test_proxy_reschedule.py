from unittest import TestCase, mock
import stager_access as stager

@mock.patch('stager_access.proxy')
class TestProxyReschedule(TestCase):
    def test_reschedule(self, proxy_mock):
        stage_id = 29
        stager.reschedule(stage_id)
        self.assertEqual(proxy_mock.LtaStager.reschedule.call_args.args[0], stage_id)

    def test_reschedule_return_status(self, proxy_mock):
        status = 'scheduled'
        proxy_mock.LtaStager.reschedule.return_value = status
        actual = stager.reschedule(11)
        self.assertEqual(actual, status)
